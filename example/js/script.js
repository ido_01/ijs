hljs.initHighlightingOnLoad();
window.ijs = new Ijs({path:'./../'},function(){
    ijs.listener.add('menu_write',menu_write);
    ijs.listener.add('menu_bar_search',menu_search);
    ijs.listener.call('project_start');
  });
function menu_search(){
    var paths = location.pathname.split('/');
    var href = '';
    for( i in paths )
      href = paths[i];
    for(i in ijs.models['menu_bar'].results)
    {
        if(ijs.models['menu_bar'].results[i].url == href)
        {
            if(i < ijs.models['menu_bar'].results.length - 1)
            {
                var key = parseInt(i) + 1;
                $('<a></a>')
                    .addClass('btn')
                    .addClass('btn_blue')
                    .addClass('right')
                    .attr('href',ijs.models['menu_bar'].results[key].url)
                    .text(ijs.models['menu_bar'].results[key].name)
                    .appendTo($('.btn_block'));
            }
            if(i > 0)
            {
              $('<a></a>')
                  .addClass('btn')
                  .attr('href',ijs.models['menu_bar'].results[i-1].url)
                  .text(ijs.models['menu_bar'].results[i-1].name)
                  .appendTo($('.btn_block'));
            }
        }
    }
    $('.navbar').find('a[href="'+href+'"]').addClass('active');
}
function menu_write(row){
    return $('<a></a>')
        .attr('href',row.url)
        .text(row.name);
}
$('body')
    .on('click','.show_demonstration_block',function(){
        var demonstration_block = $(this).closest('.demonstration_block');
        $(this).addClass('hide');
        demonstration_block.find('.hide_demonstration_block').removeClass('hide');
    })
    .on('click','.pagination_demonstration_block div',function(){
        $(this).closest('.pagination_demonstration_block').find('div').removeClass('active');
        $(this).addClass('active');
        var demonstration_block = $(this).closest('.demonstration_block');
        demonstration_block.find('.one_example').addClass('hide');
        demonstration_block.find('.one_example[example-name='+$(this).attr('for')+']').removeClass('hide');
    })
    ;
