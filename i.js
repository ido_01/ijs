function Ijs(conf,callback = null) {
    this.conf = {
      src:'src/',
      path:conf.path?conf.path:'/js/ijs/',
      dom:conf.dom?conf.dom:$('body')
    };
    this.models = {};
    this.listener = new Ijs_listener();
    this.loader = new Ijs_load(this.conf.path);
    this.callback = callback;
    document.addEventListener("DOMContentLoaded", this.Run);
}
Ijs.prototype.Run = function() {
    var _this = window.ijs;
    var scripts = [
        _this.conf.path+_this.conf.src+'crud.js',
        _this.conf.path+_this.conf.src+'search.js',
        _this.conf.path+_this.conf.src+'analyzer.js',
        _this.conf.path+_this.conf.src+'view.js',
        _this.conf.path+_this.conf.src+'event.js'
    ];
    _this.loader.add(scripts, 'ijs_src_load');
    _this.listener.add('ijs_src_load',_this.load);
}
Ijs.prototype.load = function() {
    var _this = window.ijs;
    _this.view = new Ijs_View();
    _this.analyzer = new Ijs_Analyzer(_this.conf.dom);
    for( i in _this.analyzer.found )
    {
        var found = _this.analyzer.found[i]
        var data = { id:found.id };
        data[found.action]={
              url:found.url,
              method:found.method,
              model:found.model,
              dom:found.dom,
              model_action:found.model_action,
              model_id:found.model_id,
              event:found.event,
              data:JSON.parse(found.data),
              dataType:found.dataType
            };
        if(!_this.models[found.id])
            _this.models[found.id] = new found.where(data);
        if(found.action == 'read')
            _this.models[found.id].view();
    }
    _this.callback();
}
//Listener
function Ijs_listener()
{
    this.wait = {};
}
Ijs_listener.prototype.add = function(from,callback)
{
    if(!this.wait[from])
        this.wait[from] = [];
    this.wait[from].push(callback);
}
Ijs_listener.prototype.call = function(from,what=null,where=null)
{
    if(this.wait[from])
    {
        for(i in this.wait[from])
            if(arguments.length < 3)
                var data = this.wait[from][i](what);
            else if(arguments.length == 3)
                var data = this.wait[from][i](what,where);
            else
                var data = this.wait[from][i](arguments);
    }
    return data;
}
//Loaders
function Ijs_load(path)
{
    $.ajaxSetup({
        cache: true
    });
    this.callbacks = {};
    this.path = path;
    this.scripts = {};
}
Ijs_load.prototype.add = function(scripts, function_name)
{
    this.callbacks[function_name] = function_name;
    for(i in scripts)
    {
        if(!this.scripts[scripts[i]])
            this.scripts[scripts[i]] = new Ijs_script(scripts[i],function_name);
        else
            this.scripts[scripts[i]].add_callback(function_name);
    }
    this.load();
}
Ijs_load.prototype.load = function()
{
    var load = this;
    for(var script in load.scripts)
        load.scripts[script].load(load.success);
}
Ijs_load.prototype.success = function()
{
    var callbacks = {};
    var load = window.ijs.loader;
    for(var script in load.scripts)
    {
        for(var key in load.scripts[script].callbacks)
        {
            var callback = load.scripts[script].callbacks[key];
            if(!callbacks[callback])
                callbacks[callback] = load.scripts[script].status;
            callbacks[callback] = callbacks[callback] > load.scripts[script].status ? load.scripts[script].status : callbacks[callback];
        }
    }
    for(var callback in callbacks)
    {
        if(callbacks[callback] == STATUS.DONE && load.callbacks[callback])
        {
            window.ijs.listener.call(callback);
            delete load.callbacks[callback];
        }
    }
}
//Scripts
function Ijs_script(name,add_from)
{
    this.status = STATUS.WAIT;
    this.name = name;
    this.callbacks = [add_from];
}
Ijs_script.prototype.add_callback = function(add_from)
{
    this.callbacks.push(add_from);
}
Ijs_script.prototype.load = function(callback)
{
    var script = this;
    if(script.status == STATUS.WAIT)
    {
        script.status = STATUS.LOAD;
        script.cachedScript(script.name).done(function(script_ajax, textStatus) {
            script.status = STATUS.DONE;
            callback();
        });
    }
}
Ijs_script.prototype.cachedScript = function(url, options) {
   options = $.extend(options || {}, {
       dataType: "script",
       cache: true,
       url: url
   });
   return jQuery.ajax(options);
};

var STATUS = {
  WAIT:1,
  LOAD:2,
  DONE:3
};
