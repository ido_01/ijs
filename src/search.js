function Ijs_Search(conf)
{
    this.id = conf.id?conf.id:0;
    delete conf.id;
    this.search_data = this.empty();
    for(i in conf)
    {
        for(key in conf[i])
            this[i+'_data'][key] = conf[i][key];
        this['_'+i]();
    }
    this.results = [];
}
Ijs_Search.prototype.empty = function()
{
    return {url:'/',method:'GET',dom:$('body'),model:'',data:'{}',dataType:'json'};
}
Ijs_Search.prototype._search = function()
{
    //this.search_data.data = JSON.parse(this.search_data.data);
    this.search_data.data_row = this.search_data.dom.find('.ijs_search_row').clone();
    this.search_data.dom.find('.ijs_search_row').remove();
    this.search();
}
Ijs_Search.prototype.search = function(scroll = true)
{
    var _this = this;
    console.log(_this.search_data.data);
    $.ajax({
        url:_this.search_data.url,
        method:_this.search_data.method,
        dataType:_this.search_data.dataType,
        data:_this.search_data.data,
        success:function(json)
        {
            json.scroll = scroll;
            window.ijs.listener.call(_this.id+'_search_before',json);
            _this.results = _this.search_data.model?json[_this.search_data.model]:json;
            _this.view(_this.results);
            window.ijs.listener.call(_this.id+'_search',json);
        },
        error:function(i,e){
          console.log(e);
        }
    });
}
Ijs_Search.prototype.view = function(models)
{
    var _this = this;
    for(i in models)
    {
        var new_row = _this.search_data.data_row.clone();
        window.ijs.view.find_replase(new_row,models[i]);
        new_row.appendTo(_this.search_data.dom);
    }
}
