function Ijs_View(){}
Ijs_View.prototype.find_replase = function(dom,model)
{
    var _this = this;
    if(dom.attr('data-name'))
    {
      _this.write(dom,model);
      return this;
    }
    dom.find('.ijs_list_block[data-name]').each(function(){
        _this.list($(this),model);
    });
    dom.find('[data-name]:not(.ijs_list_block)').each(function(){
        if($(this).closest('.ijs_list_block').length == 0)
          _this.write($(this),model);
    });
}
Ijs_View.prototype.list = function(dom,model)
{
    var _this = this;
    var col_name = dom.attr('data-name').split('?');
    var models = _this.read_path(model,col_name[0]);
    if(!models && col_name.length == 2)
        models = _this.read_path(model,col_name[1]);
    if(dom.find('.ijs_list_row').attr('display'))
        var display = dom.find('.ijs_list_row').attr('display');
    else
        var display = dom.find('.ijs_list_row').css('display');
    var row = dom.find('.ijs_list_row').clone();
    dom.find('.ijs_list_row').remove();

    for(i in models)
    {
        var new_row = row.clone().removeClass('ijs_list_row').css({'display':display});
        window.ijs.view.find_replase(new_row,models[i]);
        new_row.appendTo(dom);
    }
    row.attr('display',display).hide().appendTo(dom);
}
Ijs_View.prototype.write = function(dom,model)
{
    var _this = this;
    var col_name = dom.attr('data-name').split('?');
    var col = _this.read_path(model,col_name[0]);
    if(!col && col_name.length == 2)
        col = _this.read_path(model,col_name[1]);
    var callback = dom.attr('data-callback');
    if(callback)
    {
        var params = dom.attr('data-params')?JSON.parse(dom.attr('data-params')):[];
        if(Object.keys(params).length == 0)
            var row = window.ijs.listener.call(callback,col,dom);
        else
            var row = window.ijs.listener.call(callback,col,dom,params);
        if(row)
            dom.html(row);
        return 0;
    }
    if(col)
        dom.html(col).val(col);
    else
        dom.html('-');
}
Ijs_View.prototype.read_path = function(model,name)
{
    var path = name.split('.');
    for(var i in path)
    {
        if(path[i] == '*')
            return model;
        if(isFinite(model))
            model = parseFloat(model);
        if(model == path[i])
            return true;
        else if(model[path[i]] || model[path[i]] == 0)
            model = model[path[i]];
        else
            return false;
    }
    return model;
}
