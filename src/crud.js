function Ijs_CRUD(conf)
{
    this.wait = 0;
    this.id = conf.id?conf.id:0;
    delete conf.id;
    this.update_data = this.empty();
    this.create_data = this.empty();
    this.delete_data = this.empty();
    this.read_data = this.empty();
    for(i in conf)
    {
        if(!this[i+'_data'])
            continue;
        for(key in conf[i])
            this[i+'_data'][key] = conf[i][key];
        this['_'+i]();
    }
    this.model = {};
}
Ijs_CRUD.prototype.conf = function(conf)
{
    this.id = conf.id?conf.id:this.id;
    delete conf.id;
    for(i in conf)
        for(key in conf[i])
            this[i+'_data'][key] = conf[i][key];
}
Ijs_CRUD.prototype.empty = function()
{
    return {url:'',method:'GET',dom:$('body'),model:'',data:{},dataType:'json',headers:{}};
}
Ijs_CRUD.prototype._delete = function(data)
{
    return this;
}
Ijs_CRUD.prototype.delete = function()
{
  var _this = this;
  $.ajax({
      url:_this.delete_data.url,
      type:_this.delete_data.method,
      dataType:_this.delete_data.dataType,
      headers:_this.delete_data.headers,
      success:function(json){
          window.ijs.listener.call(_this.delete_data.id+'_delete');
      }
  });
}
Ijs_CRUD.prototype._create = function(data)
{
    var _this = this;
    this.create_data.dom.find('textarea[name],input[name]').each(function(){
        $(this).focusout(function(){
            _this.write($(this));
          });
    });
    return this;
}
Ijs_CRUD.prototype.create = function()
{
    var _this = this;
    this.wait++;
    $.ajax({
        url:_this.create_data.url,
        type:_this.create_data.method,
        dataType:_this.create_data.dataType,
        data:_this.model,
        headers:_this.create_data.headers,
        success:function(json){
            _this.wait--;
            window.ijs.listener.call(_this.id+'_create',json);
        },
        error:function(i){
            window.ijs.listener.call(_this.id+'_create_fail',i.responseJSON);
            _this.wait--;
        }
    });
}
Ijs_CRUD.prototype._update = function()
{
    var _this = this;
    this.update_data.dom.find('textarea[name],input[name]').each(function(){
        $(this).change(function(){
            _this.write($(this));
          });
    });
    return this;
}
Ijs_CRUD.prototype.update = function()
{
    var _this = this;
    this.wait++;
    $.ajax({
        url:_this.update_data.url,
        type:_this.update_data.method,
        data:_this.model,
        dataType:_this.update_data.dataType,
        headers:_this.update_data.headers,
        success:function(json)
        {
            _this.wait--;
            window.ijs.listener.call(_this.id+'_update',json);
        },
        error:function(){
            _this.wait--;
        }
    });
    return this;
}
Ijs_CRUD.prototype._read = function()
{
    //this.read_data.data = JSON.parse(this.read_data.data);
    if(this.read_data.url)
        this.read();
    return this;
}
Ijs_CRUD.prototype.read = function()
{
    var _this = this;
    this.wait++;
    $.ajax({
        url:_this.read_data.url,
        headers:_this.read_data.headers,
        type:_this.read_data.method,
        dataType:_this.read_data.dataType,
        data:_this.read_data.data,
        success:function(json)
        {
            window.ijs.listener.call(_this.id+'_read_before',json);
            _this.model = _this.read_data.model?json[_this.read_data.model]:json;
            _this.wait--;
            window.ijs.listener.call(_this.id+'_read',json);
        },
        error:function(){
            _this.wait--;
        }
      });
    return this;
}
Ijs_CRUD.prototype.view = function(dom = null)
{
    if(this.wait != 0)
    {
        var _this = this
        setTimeout(function(){
            _this.view(dom);
          },500);
        return this;
    }
    dom = dom?dom:this.read_data.dom;
    window.ijs.view.find_replase(dom,this.model);
    window.ijs.listener.call(this.id+'_view');
    return this;
}
Ijs_CRUD.prototype.write = function(e)
{
    var name = e.attr('name').split('.');
    if(e.attr('type') != 'radio' && e.attr('type') != 'checkbox')
        var val = e.val();
    else if($(this).prop("checked"))
        var val = e.val();
    else
        var val = 0;
    var temp = val;
    for(var i = name.length - 1; i >= 0; i--)
    {
        var ntemp = {};
        ntemp[name[i]] = temp;
        temp = ntemp;
    }
    this.model = this.assign(this.model,temp);
}
Ijs_CRUD.prototype.assign = function(to,from)
{
    for(i in from)
    {
        if(from[i] instanceof Object )
        {
            if(!to[i])
                to[i] = {};
            to[i] = this.assign(to[i],from[i]);
        }
        else
        {
            to[i] = from[i];
        }
    }
    return to;
}
