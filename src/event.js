function Ijs_Event(conf)
{
    this.id = conf.id?conf.id:0;
    delete conf.id;
    this.add_data = this.empty();
    for(i in conf)
        for(key in conf[i])
            this[i+'_data'][key] = conf[i][key];
    if(!this.add_data.callback)
        this.add_data.callback = this.add_data.model_id+'_event';
    this.wait();
}
Ijs_Event.prototype.empty = function()
{
    return {event:'click',model_id:'',dom:$('body'),model_action:'read'};
}
Ijs_Event.prototype.wait = function()
{
    var _this = this;
    _this.add_data.dom.on(_this.add_data.event,function(){
        window.ijs.listener.call(_this.add_data.callback,$(this));
        window.ijs.models[_this.add_data.model_id][_this.add_data.model_action]();
    });
}
